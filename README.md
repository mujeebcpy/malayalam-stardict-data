This is a stardict data source for malayalam dictionaries. database is based on olam project. original data available [here] (https://github.com/knadh/datuk)
this data can be used in computer and mobile dictionaries which supports startdict

## install in stardict or qstardict (computer)
clone the repository

`git clone https://gitlab.com/mujeebcpy/malayalam-stardict-data.git`

enter to the source folder

`cd malayalam-stardict-data/stardict`

copy the files into dictionary folder

`cp * ~/.stardict/dic`

## install in colordict (Mobile)
* download the 3 files in stardict folder
* copy these files into a newfolder
* copy newfolder to phone memory/dictdata

